FROM node:14.11-stretch
LABEL maintainer "John Williams"
LABEL contact "john@elfinjohn.com"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get install -y nano sudo openssl &&\
    rm -rf /var/lib/apt/lists/*

RUN mkdir /app && chown -R www-data /app
RUN mkdir /app/node_modules && chown -R www-data /app/node_modules
RUN mkdir /home/www-data && chown -R www-data /home/www-data && usermod -d /home/www-data --shell /bin/bash www-data

RUN wget -O /usr/local/bin/ttyd https://github.com/tsl0922/ttyd/releases/download/1.6.1/ttyd_linux.x86_64 && \
    chmod +x /usr/local/bin/ttyd

RUN echo "www-data ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

USER www-data

WORKDIR /app

# React app
EXPOSE 3000
# TTY app
EXPOSE 3001
# Enables hot reloading
EXPOSE 35729

ENTRYPOINT ["ttyd", "-p", "3001"]
CMD ["/bin/bash"]
